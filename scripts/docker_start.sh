#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )


# Container run
CONTAINER_ID=$(docker run -it --rm -d --privileged \
    --network=host  \
    --user=$(id -u $USER):$(id -g $USER) \
    --env="DISPLAY" \
    --env="LIBGL_ALWAYS_SOFTWARE=0" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume="$SCRIPT_DIR/..":/module_5 \
    px4_sim)

# Run gazebo
docker exec -it $CONTAINER_ID /bin/bash -c "source /opt/ros/noetic/setup.bash && \
                                            source /PX4-Autopilot/Tools/setup_gazebo.bash \
                                            /PX4-Autopilot /PX4-Autopilot/build/px4_sitl_default && \
                                            export ROS_PACKAGE_PATH=/opt/ros/noetic/share:/PX4-Autopilot:/PX4-Autopilot/Tools/sitl_gazebo && \
                                            export GAZEBO_MODEL_PATH=/app/sim/models:/PX4-Autopilot/Tools/sitl_gazebo/models
                                            roslaunch px4 mavros_posix_sitl.launch"

echo "STOP DOCKER CONTAINER!"
docker stop $CONTAINER_ID
